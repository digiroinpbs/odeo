package main

import (
	"github.com/magiconair/properties"
	"github.com/julienschmidt/httprouter"
	"log"
	"net/http"
	"io/ioutil"
	"github.com/buger/jsonparser"
	//"github.com/go-resty/resty"
	"github.com/go-redis/redis"
	"github.com/go-resty/resty"
)

func initRedis() *redis.Client{
	p := properties.MustLoadFile("/etc/service.conf", properties.UTF8)
	address := p.GetString("redis.name","localhost")+":"+p.GetString("redis.port","6379")
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: p.GetString("redis.password",""),
		DB:       p.GetInt("redis.db",0),
	})
	return client
}

func getParameter()map[string]string{
	p := properties.MustLoadFile("/etc/service.conf", properties.UTF8)
	secret:=p.GetString("odeo.secret","Bearer NDQ2NjU6JDJ5JDEwJG5UYTZqLmhGQzc4OGhOVFA5NjJISE8wT2VMZ1hhVklsbVRUOW5RWXhUeVB5VXZGOFJDVU55")
	return map[string]string{
		"Authorization": secret,
		"Content-Type": "application/json",
	}
}

func main() {
	router := httprouter.New()
	//router.POST("/checksaldo", checkSaldo)
	//router.POST("/saldodeposit", saldoDeposit)
	//router.POST("/checkharga", checkHarga)
	//router.POST("/checktransaction", checkTransaction)
	router.POST("/buypulsa", buyPulsa)
	//router.POST("/buytoken", buyToken)
	log.Fatal(http.ListenAndServe(":7052", router))
}

//contoh input
//{
//"phone":"081213631232",
//"type":"telkomsel",
//"denom":"5000"
//}
func buyPulsa(w http.ResponseWriter, r *http.Request, ps httprouter.Params){
	body, _ := ioutil.ReadAll(r.Body)
	phone, _ := jsonparser.GetString(body, "Phone")
	typePulse, _:= jsonparser.GetString(body, "Type")
	denom, _:= jsonparser.GetString(body, "Denom")
	client := initRedis()
	exist := client.Get("odeo:"+typePulse+":"+denom)
	if exist.Val()==""{
		http.Error(w, `{"error":"denom `+denom+` not exist for `+typePulse+`"}`, 404)
	}else {
		bodyReq := "{\"data\": {\"denom\": \"" + exist.Val() + "\",\"number\": \"" + phone + "\"}}"
		res, err := resty.R().SetHeaders(getParameter()).
			SetBody(bodyReq).Post("https://api.odeo.co.id/v1/affiliate/prepaid/purchase")
		status, _ := jsonparser.GetString(res.Body(), "status")
		if err == nil {
			if status == "SUCCESS" {
				w.Write(res.Body())
			} else {
				s,_ :=jsonparser.ParseString(res.Body())
				http.Error(w, s, 500)
			}
		} else {
			http.Error(w, err.Error(), 500)
		}
	}
}
